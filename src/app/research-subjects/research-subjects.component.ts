import { Component, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import { DataserviceService } from '../dataservice.service'
import { ResearchSubject } from '../models/ResearchSubject';
import { Identifier } from '../models/Identifier';
import { Period } from '../models/Period';
import { Reference } from '../models/Reference';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-research-subjects',
  templateUrl: './research-subjects.component.html',
  styleUrls: ['./research-subjects.component.scss']
})
export class ResearchSubjectsComponent implements OnInit {

  constructor(private service: DataserviceService, private route: ActivatedRoute) { }

  researchSubjectArr$: ResearchSubject[] = null;

  other: Reference = new Reference(null, null,null,null,null);
  period: Period = new Period(new Date(Date.now()),new Date(Date.now()));
  identifier: Identifier = new Identifier(null,null);
  selectedResearchSubject: ResearchSubject = new ResearchSubject(null,this.identifier,"candit",this.period,this.other,null);

  ngOnInit() {
    this.getResearchSubjects();
    this.selectedResearchSubject.id = this.route.snapshot.paramMap.get('id');
  }

  getResearchSubjects() {
    this.service.getResearchSubjects()
      .subscribe((data: ResearchSubject[]) => {
        console.log(data);
        console.log("HILFEEE");
        this.researchSubjectArr$ = data
      }
      );
  }

  //Wird aufgerufen, wenn ein ResearchSubject in der Liste angeklickt wird
  selectResearchSubject(selected: ResearchSubject) {
    console.log("clicked ResearchSubject: " + selected.id);
    this.selectedResearchSubject = selected;
  }

  onResearchSubjectModified(hideResearchSubject: boolean) {
    console.log("ResearchSubject modified " + hideResearchSubject);
    if (hideResearchSubject) { this.selectedResearchSubject = null; } 
    this.getResearchSubjects()
  }

  createResearchSubject() {
    var other: Reference = new Reference(null, null,null,null,null);
    //var other: Reference = new Reference("reference12333", "value_reference","google.at",null,"display")
    //https://stackoverflow.com/questions/41355768/argument-of-type-date-is-not-assignable-to-parameter-of-type-string
    var period: Period = new Period(new Date(Date.now()),new Date(Date.now()));
    var identifier: Identifier = new Identifier(null,null);
    var newResearchSubject: ResearchSubject =
      new ResearchSubject(null,identifier,"candit",period,other,null);
    this.service.addResearchSubject(newResearchSubject).subscribe(researchSubject => {
      console.log("ResearchSubject created")
      this.onResearchSubjectModified(true)
    });
  }

}
