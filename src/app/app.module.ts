import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PatientsComponent } from './patients/patients.component';
import { PatientDetailsComponent } from './patient-details/patient-details.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import{ DataserviceService} from'./dataservice.service';
import{ HttpClientModule} from'@angular/common/http';
import { PractitionersComponent } from './practitioners/practitioners.component';
import { PractitionerDetailsComponent } from './practitioner-details/practitioner-details.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FlagsComponent } from './flags/flags.component';
import { FlagDetailsComponent } from './flag-details/flag-details.component';
import { PractitionerRolesComponent } from './practitioner-roles/practitioner-roles.component';
import { PractitionerRoleDetailsComponent } from './practitioner-role-details/practitioner-role-details.component';
import { ResearchSubjectsComponent } from './research-subjects/research-subjects.component'
import { ResearchSubjectDetailsComponent } from './research-subject-details/research-subject-details.component'

//damit sich die KOmponenten gegenseitig kennen
@NgModule({
  declarations: [
    AppComponent,
    PatientsComponent,
    PatientDetailsComponent,
    SidebarComponent,
    PractitionersComponent,
    PractitionerDetailsComponent,
    FlagsComponent,
    FlagDetailsComponent,
    PractitionerRolesComponent,
    PractitionerRoleDetailsComponent,
    ResearchSubjectsComponent,
    ResearchSubjectDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule, 
    ReactiveFormsModule
  ],
  providers: [DataserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
