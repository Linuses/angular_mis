import { Component, OnInit, Input, OnChanges, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { DataserviceService } from "../dataservice.service";
import { PractitionerModel } from "../models/PractitionerModel";
import {  HumanName } from "../models/HumanName"
import { FormGroup, FormControl, FormArray,FormBuilder,Validators} from '@angular/forms'

@Component({
  selector: 'app-practitioner-details',
  templateUrl: './practitioner-details.component.html',
  styleUrls: ['./practitioner-details.component.scss']
})
export class PractitionerDetailsComponent implements OnInit,OnChanges {

  constructor(private service: DataserviceService, private formBuilder: FormBuilder) {
    this.createPractitionerForm();
   }

  @Input()
  id: string;

  @Output()
  practitionerModified = new EventEmitter<boolean>(); //wenns geändert wird, wirds aktualisiert

  practitioner: PractitionerModel = null;

  public practitionerForm: FormGroup;

  ngOnInit(): void {
    //this.getPractitionerDetail();
  }

  /* import("@angular/core").ngOnChanges(changes: SimpleChanges): void { */

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.getPractitionerDetail();
  }

  get namesArray() {
    return this.practitionerForm.get("name") as FormArray;
  }
  set namesArray(namesArray: FormArray) {
    this.practitionerForm.controls.name = namesArray;
  }


  get qualificationsArray() {
    return this.practitionerForm.get("qualification") as FormArray;
  }
  set qualificationsArray(qualifcationsArray: FormArray) {
    this.practitionerForm.controls.qualification = qualifcationsArray;
  }

  createPractitionerForm() {
    this.practitionerForm = this.formBuilder.group({
      active:[""],
      gender:["unknown"],
      deceasedBoolean: [""],
      deceasedDateTime:[""],
      birthDate:[""],
      name:this.formBuilder.array([]),
      qualification:this.formBuilder.array([]),
    })
  }

  updatePractitionerForm() {
    this.practitionerForm.controls.active.setValue(this.practitioner.active);
    this.practitionerForm.controls.gender.setValue(this.practitioner.gender);
    this.practitionerForm.controls.deceasedDateTime.setValue(this.practitioner.deceasedDateTime);
    this.practitionerForm.controls.deceasedBoolean.setValue(this.practitioner.deceasedBoolean);
    this.practitionerForm.controls.birthDate.setValue(this.practitioner.birthDate);

    this.clearFormArray(this.namesArray);
    this.practitioner.name.forEach((name)=>{
      console.log("pushname "+ name.family);
      this.namesArray.push(
        this.formBuilder.group({
          id:[name.id],
          text:[name.text],
          use:[name.use],
          family:[name.family], 
      }));
    });

    this.clearFormArray(this.qualificationsArray);
    this.practitioner.qualification.forEach((qualification)=>{
      console.log("pushname " + qualification.id);
      this.qualificationsArray.push(
        this.formBuilder.group({
          id:[qualification.id],
          PE_START:[qualification.period.start],
          PE_END:[qualification.period.end],
      }));
    });
  } 

  public addName() {
    this.namesArray.push(this.createName());
  }
  createName(): FormGroup {
    return this.formBuilder.group({
      id: [""],
      use:["official"],
      text:[""],
      family:[""],
    });
  }

  public addQualification() {
    this.qualificationsArray.push(this.createQualification());
  }
  createQualification(): FormGroup {
    return this.formBuilder.group({
      id: [""],
      PE_START: [""],
      PE_END: [""],
    });
  }

  clearFormArray = (formArray:FormArray)=>{
    while(formArray.length!==0){
      formArray.removeAt(0)
    }
  }

  onSubmitUpdate() {
    console.log("Update from formdata" + this.practitionerForm.value);
    this.practitioner.active=this.practitionerForm.value.active;
    this.practitioner.gender=this.practitionerForm.value.gender;
    this.practitioner.birthDate=this.practitionerForm.value.birthDate;
    this.practitioner.deceasedDateTime=this.practitionerForm.value.deceasedDateTime;
    this.practitioner.deceasedBoolean=this.practitionerForm.value.deceasedBoolean;
    this.practitioner.name=this.practitionerForm.value.name;
    this.practitioner.communication=this.practitionerForm.value.communication;
    this.practitioner.photo=this.practitionerForm.value.phot;
    this.practitioner.qualification=this.practitionerForm.value.qualification;
    this.practitioner.resourceType=this.practitionerForm.value.resourceType;
    this.practitioner.telecom=this.practitionerForm.value.telecom;
    this.practitioner.multipleBirthBoolean=this.practitionerForm.value.multipleBirthBoolean;
    this.practitioner.multipleBirthInteger=this.practitionerForm.value.multipleBirthInteger;
    this.updatePractitioner();
  }

  getPractitionerDetail() {
    this.service.getPractitionerDetail(this.id)
      .subscribe((data: PractitionerModel) => {
        console.log(data);
        this.practitioner = data;
        this.updatePractitionerForm();
      });
  }

  deletePractitioner() {
    this.service.deletePractitioner(this.practitioner.id)
      .subscribe(x => this.practitionerModified.emit(true));
  }

  updatePractitioner() {
    var newPractitioner: PractitionerModel = this.practitioner; 
    this.service.updatePractitioner(
      newPractitioner)
      .subscribe((practitioner) => {
        console.log("Practitioner updated");
        this.practitioner = practitioner;
        this.practitionerModified.emit(false);
      });
  }
}