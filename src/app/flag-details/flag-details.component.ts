import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { DataserviceService } from "../dataservice.service";
import { Flag } from "../models/Flag";
import { HumanName } from "../models/HumanName"
import { FormGroup, FormControl, FormArray,FormBuilder,Validators} from '@angular/forms'



@Component({
  selector: 'app-flag-details',
  templateUrl: './flag-details.component.html',
  styleUrls: ['./flag-details.component.scss']
})
export class FlagDetailsComponent implements OnInit, OnChanges {

  constructor(private service: DataserviceService,
    private formBuilder: FormBuilder) {
      this.createFlagForm();
  }

  @Input()
  id: string;

  @Output()
  flagModified = new EventEmitter<boolean>();

  flag: Flag = null;

  public flagForm: FormGroup;

  ngOnInit(): void {
  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.getFlagDetail();
  }

  createFlagForm() {
    this.flagForm = this.formBuilder.group({
      status:["inactive"],
      periodStart:[""],
      periodEnd:[""],
      //reference:[''],
    })
  }

  updateFlagForm() {
    this.flagForm.controls.status.setValue(this.flag.status);
    this.flagForm.controls.periodEnd.setValue(this.flag.period.end);
    this.flagForm.controls.periodStart.setValue(this.flag.period.start);
    //this.flagForm.controls.reference.setValue(this.flag.other.reference);
  } 

  onSubmitUpdate() {
    console.log("Update from formdata" + this.flagForm.value);
    this.flag.status=this.flagForm.value.status;
    this.flag.period.start=this.flagForm.value.periodStart;
    this.flag.period.end=this.flagForm.value.periodEnd;
    //this.flag.other.reference = this.flagForm.value.reference;
    this.updateFlag();
  }

  getFlagDetail() {
    this.service.getFlagDetail(this.id)
      .subscribe((data: Flag) => {
        console.log(data);
        this.flag = data;
        this.updateFlagForm();
      });
  }

  deleteFlag() {
    this.service.deleteFlag(this.flag.id)
      .subscribe((x) => this.flagModified.emit(true));
  }

  updateFlag() {
    var newFlag: Flag = this.flag; 
    this.service.updateFlag(
      newFlag)
      .subscribe((flag) => {
        console.log("Flag updated");
        this.flag = flag;
        this.flagModified.emit(false);
      });
  }

}
