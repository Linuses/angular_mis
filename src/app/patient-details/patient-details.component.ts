import { Component, OnInit, Input, OnChanges, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { DataserviceService } from "../dataservice.service";
import { PatientModel } from "../models/PatientModel";
import { HumanName } from "../models/HumanName"
import { FormGroup, FormControl, FormArray,FormBuilder,Validators} from '@angular/forms'

@Component({
  selector: 'app-patient-details',
  templateUrl: './patient-details.component.html',
  styleUrls: ['./patient-details.component.scss']
})
export class PatientDetailsComponent implements OnInit, OnChanges {

  constructor(private service: DataserviceService,
    private formBuilder: FormBuilder) {
      this.createPatientForm();
  }


  @Input()
  id: string;

  @Output()
  patientModified = new EventEmitter<boolean>();

  patient: PatientModel = null;

  public patientForm: FormGroup;

  ngOnInit(): void {
    //this.getPatientDetail();
  }

  /* import("@angular/core").ngOnChanges(changes: SimpleChanges): void { */

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.getPatientDetail();
  }

  get namesArray() {
    return this.patientForm.get("name") as FormArray;
  }
  set namesArray(namesArray: FormArray) {
    this.patientForm.controls.name = namesArray;
  }

  createPatientForm() {
    this.patientForm = this.formBuilder.group({
      active:[""],
      gender:["unknown"],
      deceasedBoolean: [""],
      deceasedDateTime:[""],
      birthDate:[""],
      name:this.formBuilder.array([]),
    })
  }

  updatePatientForm() {
    this.patientForm.controls.active.setValue(this.patient.active);
    this.patientForm.controls.gender.setValue(this.patient.gender);
    this.patientForm.controls.deceasedDateTime.setValue(this.patient.deceasedDateTime);
    this.patientForm.controls.deceasedBoolean.setValue(this.patient.deceasedBoolean);
    this.patientForm.controls.birthDate.setValue(this.patient.birthDate);
    this.clearFormArray(this.namesArray);

    this.patient.name.forEach((name)=>{
      console.log("pushname "+name.family);
      this.namesArray.push(
        this.formBuilder.group({
          id:[name.id],
          text:[name.text],
          use:[name.use],
          family:[name.family],
        }));
      });

  } 

  public addName() {
    this.namesArray.push(this.createName());
  }
  createName(): FormGroup {
    return this.formBuilder.group({
      id: [""],
      use:["official"],
      text:[""],
      family:[""],
    });
  }

  clearFormArray = (formArray:FormArray)=>{
    while(formArray.length!==0){
      formArray.removeAt(0)
    }
  }

  onSubmitUpdate() {
    console.log("Update from formdata" + this.patientForm.value);
    this.patient.active=this.patientForm.value.active;
    this.patient.gender=this.patientForm.value.gender;
    this.patient.birthDate=this.patientForm.value.birthDate;
    this.patient.deceasedDateTime=this.patientForm.value.deceasedDateTime;
    this.patient.deceasedBoolean=this.patientForm.value.deceasedBoolean;
    this.patient.name=this.patientForm.value.name;
    this.updatePatient();
  }

  getPatientDetail() {
    this.service.getPatientDetail(this.id)
      .subscribe((data: PatientModel) => {
        console.log(data);
        this.patient = data;
        this.updatePatientForm();
      });
  }

  deletePatient() {
    this.service.deletePatient(this.patient.id)
      .subscribe((x) => this.patientModified.emit(true));
  }

  updatePatient() {
    var newPatient: PatientModel = this.patient; 
    this.service.updatePatient(
      newPatient)
      .subscribe((patient) => {
        console.log("Patient updated");
        this.patient = patient;
        this.patientModified.emit(false);
      });
  }
}