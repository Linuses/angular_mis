import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResearchSubjectDetailsComponent } from './research-subject-details.component';

describe('ResearchSubjectDetailsComponent', () => {
  let component: ResearchSubjectDetailsComponent;
  let fixture: ComponentFixture<ResearchSubjectDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResearchSubjectDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResearchSubjectDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
