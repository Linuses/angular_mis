import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { DataserviceService } from "../dataservice.service";
import { ResearchSubject } from "../models/ResearchSubject";
import { HumanName } from "../models/HumanName"
import { FormGroup, FormControl, FormArray,FormBuilder,Validators} from '@angular/forms'

@Component({
  selector: 'app-research-subject-details',
  templateUrl: './research-subject-details.component.html',
  styleUrls: ['./research-subject-details.component.scss']
})
export class ResearchSubjectDetailsComponent implements OnInit {

  constructor(private service: DataserviceService,
    private formBuilder: FormBuilder) {
      this.createResearchSubjectForm();
  }

  @Input()
  id: string;

  @Output()
  researchSubjectModified = new EventEmitter<boolean>();

  researchSubject: ResearchSubject = null;

  public researchSubjectForm: FormGroup;

  ngOnInit(): void {
  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.getResearchSubjectDetail();
  }

  createResearchSubjectForm() {
    this.researchSubjectForm = this.formBuilder.group({
      rsDatetime:[""],
      periodStart:[""],
      periodEnd:[""],
      referenceDisplay:[""],
    })
  }

  updateResearchSubjectForm() {
    this.researchSubjectForm.controls.periodEnd.setValue(this.researchSubject.period.end);
    this.researchSubjectForm.controls.periodStart.setValue(this.researchSubject.period.start);
    this.researchSubjectForm.controls.rsDatetime.setValue(this.researchSubject.rs_datetime);
    this.researchSubjectForm.controls.referenceDisplay.setValue(this.researchSubject.individualization.display);
  } 

  onSubmitUpdate() {
    console.log("Update from formdata" + this.researchSubjectForm.value);
    this.researchSubject.period.start=this.researchSubjectForm.value.periodStart;
    this.researchSubject.period.end=this.researchSubjectForm.value.periodEnd;
    this.researchSubject.rs_datetime=this.researchSubjectForm.value.rsDatetime;
    this.researchSubject.individualization.display=this.researchSubjectForm.value.referenceDisplay;
    this.updateResearchSubject();
  }

  getResearchSubjectDetail() {
    this.service.getResearchSubjectDetail(this.id)
      .subscribe((data: ResearchSubject) => {
        console.log(data);
        this.researchSubject = data;
        this.updateResearchSubjectForm(); 
      });
  }

  deleteResearchSubject() {
    this.service.deleteResearchSubject(this.researchSubject.id)
      .subscribe((x) => this.researchSubjectModified.emit(true));
  }

  updateResearchSubject() {
    var newResearchSubject: ResearchSubject = this.researchSubject; 
    this.service.updateResearchSubject(
      newResearchSubject)
      .subscribe((researchSubject) => {
        console.log("ResearchSubject updated");
        this.researchSubject = researchSubject;
        this.researchSubjectModified.emit(false);
      });
  }

}
