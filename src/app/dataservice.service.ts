import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { PatientModel } from './models/PatientModel';
import { PractitionerModel } from './models/PractitionerModel';
import { PractitionerRole } from './models/PractitionerRole';
import { ResearchSubject } from './models/ResearchSubject';
import { Flag } from './models/Flag';
import { catchError } from 'rxjs/operators';

const patientsUrl: string = "http://localhost:8080/api/patient/";
const practitionersUrl: string = "http://localhost:8080/api/practitioner/";
const flagsUrl: string = "http://localhost:8080/api/flag/";
const practitionerRolesUrl: string = "http://localhost:8080/api/practitionerRole/";
const researchSubjectsUrl: string = "http://localhost:8080/api/researchSubject/";
const httpOptions = { 
  headers: new HttpHeaders({ 
    'Content-Type': 'application/json' 
  }) 
};

//Kommunikationsglied zwischen Client und Server
//holt sich die JSONs vom Server und schickt die JSONs
@Injectable({
  providedIn: 'root'
})
export class DataserviceService{
  constructor(private http:HttpClient) { } 

  
  //Patient
  public getPatients(): Observable <PatientModel[] > {
    console.log("getPatients called"); 
    return this.http.get<PatientModel[]>(patientsUrl)
    .pipe(
      catchError(this.handleError('getPatients', []))
      );
  }

  public getPatientDetail (id: string): Observable < PatientModel > { 
    console.log("getPatientDetail called"); 
    return this.http.get<PatientModel>(patientsUrl+ id)
    .pipe(
      catchError(this.handleError('getPatientDetail', null))
      ); 
  }

  public deletePatient(id:string): Observable <{}> {
    return this.http.delete(patientsUrl+id,httpOptions)
    .pipe(
      catchError(this.handleError('deletePatient'))
    );
  }

  public addPatient(patient:PatientModel):Observable<PatientModel>{
    return this.http.post<PatientModel>
    (patientsUrl,patient,httpOptions)
    .pipe(
      catchError(this.handleError('addPatient',patient))
    );
  }

  public updatePatient(patient:PatientModel):Observable<PatientModel>{
    return this.http.put<PatientModel>
    (patientsUrl+patient.id,patient,httpOptions)
    .pipe(
      catchError(this.handleError('updatePatient',patient)));}





  //Practitioner
  public getPractitioners(): Observable <PractitionerModel[] > {
    console.log("getPractitioners called"); 
    return this.http.get<PractitionerModel[]>(practitionersUrl)
    .pipe(
      catchError(this.handleError('getPractitioners', []))
      );
  }
  public getPractitionerDetail (id: String): Observable < PractitionerModel > { 
    return this.http.get<PractitionerModel>(practitionersUrl + id)
    .pipe(
      catchError(this.handleError('getPractitionerDetail', null))
      ); 
  }

  public deletePractitioner(id:string): Observable <{}> {
    return this.http.delete(practitionersUrl+id,httpOptions)
    .pipe(
      catchError(this.handleError('deletePractitioner'))
    );
  }

  public addPractitioner(practitioner:PractitionerModel):Observable<PractitionerModel>{
    return this.http.post<PractitionerModel>
    (practitionersUrl,practitioner,httpOptions)
    .pipe(
      catchError(this.handleError('addPractitioner',practitioner))
    );
  }

  public updatePractitioner(practitioner:PractitionerModel):Observable<PractitionerModel>{
    return this.http.put<PractitionerModel>
    (practitionersUrl+practitioner.id,practitioner,httpOptions)
    .pipe(
      catchError(this.handleError('updatePractitioner',practitioner)));}



  //Flag
  public getFlags(): Observable <Flag[] > {
    console.log("getFlags called"); 
    return this.http.get<Flag[]>(flagsUrl)
    .pipe(
      catchError(this.handleError('getFlags', []))
      );
  }
  public getFlagDetail (id: String): Observable < Flag > { 
    return this.http.get<Flag>(flagsUrl + id)
    .pipe(
      catchError(this.handleError('getFlagDetail', null))
      ); 
  }

  public deleteFlag(id:string): Observable <{}> {
    return this.http.delete(flagsUrl+id,httpOptions)
    .pipe(
      catchError(this.handleError('deleteFlag'))
    );
  }

  public addFlag(flag:Flag):Observable<Flag>{
    return this.http.post<Flag>
    (flagsUrl,flag,httpOptions)
    .pipe(
      catchError(this.handleError('addFlag',flag))
    );
  }

  public updateFlag(flag:Flag):Observable<Flag>{
    return this.http.put<Flag>
    (flagsUrl+flag.id,flag,httpOptions)
    .pipe(
      catchError(this.handleError('updateFlag',flag)));}


  //PractitionerRole
  public getPractitionerRoles(): Observable <PractitionerRole[] > {
    console.log("getPractitionerRoles called"); 
    return this.http.get<PractitionerRole[]>(practitionerRolesUrl)
    .pipe(
      catchError(this.handleError('getPractitionerRoles', []))
      );
  }

  public getPractitionerRoleDetail (id: String): Observable < PractitionerRole > { 
    return this.http.get<PractitionerRole>(practitionerRolesUrl + id)
    .pipe(
      catchError(this.handleError('getPractitionerRoleDetail', null))
      ); 
  }

  public deletePractitionerRole(id:string): Observable <{}> {
    return this.http.delete(practitionerRolesUrl+id,httpOptions)
    .pipe(
      catchError(this.handleError('deletePractitionerRole'))
    );
  }

  public addPractitionerRole(PractitionerRole:PractitionerRole):Observable<PractitionerRole>{
    return this.http.post<PractitionerRole>
    (practitionerRolesUrl,PractitionerRole,httpOptions)
    .pipe(
      catchError(this.handleError('addPractitionerRole',PractitionerRole))
    );
  }

  public updatePractitionerRole(PractitionerRole:PractitionerRole):Observable<PractitionerRole>{
    return this.http.put<PractitionerRole>
    (practitionerRolesUrl+PractitionerRole.id,PractitionerRole,httpOptions)
    .pipe(
      catchError(this.handleError('updatePractitionerRole',PractitionerRole)));}

  //ResearchSubject
  public getResearchSubjects(): Observable <ResearchSubject[] > {
    console.log("getResearchSubjects called"); 
    return this.http.get<ResearchSubject[]>(researchSubjectsUrl)
    .pipe(
      catchError(this.handleError('getResearchSubjects', []))
      );
  }

  public getResearchSubjectDetail (id: String): Observable < ResearchSubject > { 
    return this.http.get<ResearchSubject>(researchSubjectsUrl +id)
    .pipe(
      catchError(this.handleError('getResearchSubjectDetail', null))
      ); 
  }

  public deleteResearchSubject(id:string): Observable <{}> {
    return this.http.delete(researchSubjectsUrl+id,httpOptions)
    .pipe(
      catchError(this.handleError('deleteResearchSubject'))
    );
  }

  public addResearchSubject(ResearchSubject:ResearchSubject):Observable<ResearchSubject>{
    return this.http.post<ResearchSubject>
    (researchSubjectsUrl,ResearchSubject,httpOptions)
    .pipe(
      catchError(this.handleError('addResearchSubject',ResearchSubject))
    );
  }

  public updateResearchSubject(ResearchSubject:ResearchSubject):Observable<ResearchSubject>{
    return this.http.put<ResearchSubject>
    (researchSubjectsUrl+ResearchSubject.id,ResearchSubject,httpOptions)
    .pipe(
      catchError(this.handleError('updateResearchSubject',ResearchSubject)));}

  private handleError<T>(operation = 'operation', result ?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); 
      // log to console instead
      // TODO: better job of transforming error for user       consumption
      console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
    return of(result as T);
    };
  }

}
