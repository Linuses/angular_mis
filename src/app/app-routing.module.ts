import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FlagsComponent } from './flags/flags.component';
import{ PatientsComponent} from'./patients/patients.component';
import{ PractitionersComponent} from'./practitioners/practitioners.component';
import{ PractitionerRolesComponent } from'./practitioner-roles/practitioner-roles.component';
import{ ResearchSubjectsComponent } from'./research-subjects/research-subjects.component';

//damit die URL auf die Componenten gemappt wird

const routes: Routes = [
  { path:'', component: PatientsComponent},
  { path:'patients', component: PatientsComponent },
  { path: 'practitioners', component: PractitionersComponent },
  { path:'flags', component: FlagsComponent},
  { path:'practitionerRoles', component: PractitionerRolesComponent},
  { path:'researchSubjects', component: ResearchSubjectsComponent},
  { path: 'patients/:id', component: PatientsComponent } ,
  { path: 'practitioners/:id', component: PractitionersComponent } ,
  { path: 'flags/:id', component: FlagsComponent } ,
  { path: 'practitionerRoles/:id', component: PractitionerRolesComponent } ,
  { path: 'researchSubjects/:id', component: ResearchSubjectsComponent } ,
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
