import { Reference } from "./Reference";
import { Identifier } from './Identifier';
import { Period } from './Period';
import { CodeableConcept } from './CodeableConcept';
import { PractitionerRoleAvailableTime } from './PractitionerRoleAvailableTime'

export class PractitionerRole{
    constructor(
        public id: string, 
        public identifier:[Identifier],
        public active: Boolean,
        public period: Period,
        public ref_practitioner: Reference,
        public speciality: [CodeableConcept],
        public availableTime: [PractitionerRoleAvailableTime]
    )
    {}
}
