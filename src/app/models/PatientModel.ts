import { HumanName } from './HumanName';
import { ContactPoint } from './ContactPoint';
import { Identifier } from './Identifier';


export class PatientModel{ 
    constructor(
        public id: string, 
        public resourceType: string, 
        public name: [ HumanName ], // A name associated with the patient
        public telecom: [ ContactPoint],
        public identifier:[Identifier],
        public active: boolean,
        public gender: string,
        public birthDate: Date,
        public deceasedBoolean:boolean,
        public deceasedDateTime:Date,
        public multipleBirthBoolean: boolean,
        public multipleBirthInteger: number,
        ) 
    {}
}