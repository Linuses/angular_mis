export class HumanName
    {

        //fields
    public id:string=''
    public use:string=""
    public text:string=''
    public family:string=''
        
        constructor(
            //parameter
            id:string='',
            use:string="",
            text:string='',
            family:string=''
        )
        {
            //werte zuweisen
            this.id=id;
            this.use=use;
            this.family=family;
            this.text=text;
        }
    }

export enum HumanName_UseCode{
    usual="usual",
    official="official",
    temp="temp",
    nickname="nickname",
    anonymous="anonymous",
    old="old",
    maiden="maiden"
}
    