import { Identifier } from "./Identifier"

export class Reference{
    constructor(
    public id:string,
    public type:string,
    public reference:string,
    public identifier:[ Identifier],
    public display:string,
    )
    {}
}