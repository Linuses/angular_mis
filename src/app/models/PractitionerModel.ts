import { HumanName } from './HumanName';
import { ContactPoint } from './ContactPoint';
import { Identifier } from './Identifier';
import { Qualification } from './Qualification';
import { Address } from './Address';
import { Attachment } from './Attachment';
import { CodeableConcept } from './CodeableConcept';

export class PractitionerModel{ 
    constructor(
        public id: string, 
        public resourceType: string, 
        public name: [ HumanName ], // A name associated with the patient
        public telecom: [ ContactPoint],

        public active: boolean,
        public gender: string,
        public birthDate: Date,
        public deceasedBoolean:boolean,
        public deceasedDateTime:Date,
        public multipleBirthBoolean: boolean,
        public multipleBirthInteger: number,
        

        public Address: [  Address ],
        public photo: [  Attachment ],
        public qualification: [  Qualification ],
        public communication: [  CodeableConcept ],

        ) 
    {}
}