
export class PractitionerRoleAvailableTime{
    constructor(
        public id: string, 
        public daysOfWeek:string,
        public allDay: Boolean,
        public availableStartTime:Date,
        public availableEndTime:Date
    )
    {}
}
