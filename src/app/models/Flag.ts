import { Reference } from "./Reference";
import { Identifier } from './Identifier';
import { Period } from './Period';

export class Flag{
    constructor(
        public id: string, 
        public identifier:[Identifier],
        public status: string,
        public other: Reference,
        public period: Period
    )
    {}
}

export enum Flag_StatusCode{
    all="all",
    between="between",
    neverdothat="never-do-that",
}