import { Reference } from "./Reference";
import { Identifier } from './Identifier';
import { Period } from './Period';

export class ResearchSubject{
    constructor(
        public id: string, 
        public identifier:Identifier,
        public status: string,
        public period: Period,
        public individualization: Reference,
        public rs_datetime:Date
    )
    {}
}