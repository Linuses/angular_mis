

export class Identifier {
    constructor(
        public id: string, 
        public value: string){
    }
}