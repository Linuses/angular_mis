import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PractitionerRoleDetailsComponent } from './practitioner-role-details.component';

describe('PractitionerRoleDetailsComponent', () => {
  let component: PractitionerRoleDetailsComponent;
  let fixture: ComponentFixture<PractitionerRoleDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PractitionerRoleDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PractitionerRoleDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
