import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { DataserviceService } from "../dataservice.service";
import { PractitionerRole } from "../models/PractitionerRole";
import { HumanName } from "../models/HumanName"
import { FormGroup, FormControl, FormArray,FormBuilder,Validators} from '@angular/forms'


@Component({
  selector: 'app-practitioner-role-details',
  templateUrl: './practitioner-role-details.component.html',
  styleUrls: ['./practitioner-role-details.component.scss']
})
export class PractitionerRoleDetailsComponent implements OnInit {

  constructor(private service: DataserviceService,
    private formBuilder: FormBuilder) {
      this.createPractitionerRoleForm();
  }

  @Input()
  id: string;

  @Output()
  practitionerRoleModified = new EventEmitter<boolean>();

  practitionerRole: PractitionerRole = null;

  public practitionerRoleForm: FormGroup;

  ngOnInit(): void {
  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.getPractitionerRoleDetail();
  }

  get atArray() {
    return this.practitionerRoleForm.get("availableTime") as FormArray;
  }
  set atArray(atArray: FormArray) {
    this.practitionerRoleForm.controls.availableTime = atArray;
  }

  get idiArray() {
    return this.practitionerRoleForm.get("identifier") as FormArray;
  }
  set idiArray(idiArray: FormArray) {
    this.practitionerRoleForm.controls.identifier = idiArray;
  }

  createPractitionerRoleForm() {
    this.practitionerRoleForm = this.formBuilder.group({
      periodStart:[""],
      periodEnd:[""],
      identifier:this.formBuilder.array([]),
      availableTime:this.formBuilder.array([]),
    })
  }

  updatePractitionerRoleForm() {
    this.practitionerRoleForm.controls.periodEnd.setValue(this.practitionerRole.period.end);
    this.practitionerRoleForm.controls.periodStart.setValue(this.practitionerRole.period.start);

    this.clearFormArray(this.idiArray);
    this.practitionerRole.identifier.forEach((identifier)=>{
      console.log("pushname "+ identifier.id);
      this.idiArray.push(
        this.formBuilder.group({
          id:[identifier.id],
          value:[identifier.value],
      }));
    });

    this.clearFormArray(this.atArray);
    this.practitionerRole.availableTime.forEach((availableTime)=>{
      console.log("pushname "+ availableTime.id);
      this.atArray.push(
        this.formBuilder.group({
          id:[availableTime.id],
          daysOfWeek:[availableTime.daysOfWeek],
          allDay:[availableTime.allDay],
          availableStartTime:[availableTime.availableStartTime],
          availableEndTime:[availableTime.availableEndTime],
      }));
    });
  } 

  public addIdentifier () {
    this.idiArray.push(this.createIdentifier());
  }

  createIdentifier(): FormGroup {
    return this.formBuilder.group({
      id: [""],
      value:["idiiiii"],
    });
  }

  public addAvailableTime () {
    this.atArray.push(this.createAvailableTime());
  }

  createAvailableTime(): FormGroup {
    return this.formBuilder.group({
      id: [""],
      daysOfWeek: ["tue,wed"],
      allDay: false,
      availableStartTime: "2021-02-20T11:54:00",
      availableEndTime: "2021-02-20T11:54:00",
    });
  }

  clearFormArray = (formArray:FormArray)=>{
    while(formArray.length!==0){
      formArray.removeAt(0)
    }
  }

  onSubmitUpdate() {
    console.log("Update from formdata" + this.practitionerRoleForm.value);
    this.practitionerRole.period.start=this.practitionerRoleForm.value.periodStart;
    this.practitionerRole.period.end=this.practitionerRoleForm.value.periodEnd;
    this.practitionerRole.identifier=this.practitionerRoleForm.value.identifier;
    this.practitionerRole.availableTime=this.practitionerRoleForm.value.availableTime;
    //this.practitionerRole.other.reference = this.practitionerRoleForm.value.reference;
    this.updatePractitionerRole();
  }

  getPractitionerRoleDetail() {
    this.service.getPractitionerRoleDetail(this.id)
      .subscribe((data: PractitionerRole) => {
        console.log(data);
        this.practitionerRole = data;
        this.updatePractitionerRoleForm();
      });
  }

  deletePractitionerRole() {
    this.service.deletePractitionerRole(this.practitionerRole.id)
      .subscribe((x) => this.practitionerRoleModified.emit(true));
  }

  updatePractitionerRole() {
    var newPractitionerRole: PractitionerRole = this.practitionerRole; 
    this.service.updatePractitionerRole(
      newPractitionerRole)
      .subscribe((practitionerRole) => {
        console.log("PractitionerRole updated");
        this.practitionerRole = practitionerRole;
        this.practitionerRoleModified.emit(false);
      });
  }

}
