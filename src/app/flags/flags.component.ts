import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { DataserviceService } from '../dataservice.service'
import { Flag } from '../models/Flag';
import { Identifier } from '../models/Identifier';
import { Period } from '../models/Period';
import { Reference } from '../models/Reference';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-flags',
  templateUrl: './flags.component.html',
  styleUrls: ['./flags.component.scss']
})
export class FlagsComponent implements OnInit {

  constructor(private service: DataserviceService, private route: ActivatedRoute) { }

  flagArr$: Flag[] = null;
  other: Reference = new Reference(null, null,null,null,null);
  period: Period = new Period(new Date(Date.now()),new Date(Date.now()));

  selectedFlag: Flag = new Flag(null,null,"all",this.other,this.period);

  ngOnInit() {
    this.getFlags();
    this.selectedFlag.id = this.route.snapshot.paramMap.get('id');
  }

  getFlags() {
    this.service.getFlags()
      .subscribe((data: Flag[]) => {
        console.log(data);
        this.flagArr$ = data
      }
      );
  }

  //Wird aufgerufen, wenn ein Flag in der Liste angeklickt wird
  selectFlag(selected: Flag) {
    console.log("clicked Flag: " + selected.id);
    this.selectedFlag = selected;
  }

  onFlagModified(hideFlag: boolean) {
    console.log("Flag modified " + hideFlag);
    if (hideFlag) { this.selectedFlag = null; } 
    this.getFlags()
  }

  createFlag() {
    let other: Reference = new Reference(null, null,null,null,null);
    //var other: Reference = new Reference("reference12333", "value_reference","google.at",null,"display")
    //https://stackoverflow.com/questions/41355768/argument-of-type-date-is-not-assignable-to-parameter-of-type-string
    let period: Period = new Period(new Date(Date.now()),new Date(Date.now()));

    let newFlag: Flag =
      new Flag(null, 
        null,
        "all",
        other, 
        period)
    this.service.addFlag(newFlag).subscribe(flag => {
      console.log("Flag created")
      this.onFlagModified(true)
    });
  }

}
