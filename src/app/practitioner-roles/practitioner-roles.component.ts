import { Component, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import { DataserviceService } from '../dataservice.service'
import { PractitionerRole } from '../models/PractitionerRole';
import { Identifier } from '../models/Identifier';
import { Period } from '../models/Period';
import { Reference } from '../models/Reference';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-practitioner-roles',
  templateUrl: './practitioner-roles.component.html',
  styleUrls: ['./practitioner-roles.component.scss']
})
export class PractitionerRolesComponent implements OnInit {

  constructor(private service: DataserviceService, private route: ActivatedRoute) { }

  practitionerRoleArr$: PractitionerRole[] = null;


    other: Reference = new Reference(null, null,null,null,null)
    period: Period = new Period(new Date(Date.now()),new Date(Date.now())) 


  selectedPractitionerRole: PractitionerRole = new PractitionerRole(null, 
        null,
        true, 
        this.period,
        this.other,null,null);

  ngOnInit() {
    this.getPractitionerRoles();
    this.selectedPractitionerRole.id = this.route.snapshot.paramMap.get('id');
  }

  getPractitionerRoles() {
    this.service.getPractitionerRoles()
      .subscribe((data: PractitionerRole[]) => {
        console.log(data);
        this.practitionerRoleArr$ = data
      }
      );
  }

  //Wird aufgerufen, wenn ein PractitionerRole in der Liste angeklickt wird
  selectPractitionerRole(selected: PractitionerRole) {
    console.log("clicked PractitionerRole: " + selected.id);
    this.selectedPractitionerRole = selected;
  }

  onPractitionerRoleModified(hidePractitionerRole: boolean) {
    console.log("PractitionerRole modified " + hidePractitionerRole);
    if (hidePractitionerRole) { this.selectedPractitionerRole = null; } 
    this.getPractitionerRoles()
  }

  createPractitionerRole() {
    var other: Reference = new Reference(null, null,null,null,null);
    //var other: Reference = new Reference("reference12333", "value_reference","google.at",null,"display")
    //https://stackoverflow.com/questions/41355768/argument-of-type-date-is-not-assignable-to-parameter-of-type-string
    var period: Period = new Period(new Date(Date.now()),new Date(Date.now()));

    var newPractitionerRole: PractitionerRole =
      new PractitionerRole(null, 
        null,
        true, 
        period,
        other,null,null);
    this.service.addPractitionerRole(newPractitionerRole).subscribe(practitionerRole => {
      console.log("PractitionerRole created")
      this.onPractitionerRoleModified(true)
    });
  }

}
