import { Component, OnInit } from '@angular/core';
import{ DataserviceService} from'../dataservice.service'
import{ PractitionerModel} from'../models/PractitionerModel';
import { ActivatedRoute } from '@angular/router'

@Component({ //wie ich das im html anspreche
  selector: 'app-practitioners',
  templateUrl: './practitioners.component.html',
  styleUrls: ['./practitioners.component.scss']
})
export class PractitionersComponent implements OnInit {

  constructor(private service: DataserviceService, private route: ActivatedRoute) { }

  practitionerArr$: PractitionerModel[] = null;
  selectedPractitioner: PractitionerModel = new PractitionerModel(null, 
        null,
        null, 
        null, 
        true, 
        "unknown",
        null, 
        false,
        null, 
        false, 
        null,
        null,
        null,
        null,
        null);

  ngOnInit(): void {
    this.getPractitioners();
    this.selectedPractitioner.id = this.route.snapshot.paramMap.get('id');
  }

  getPractitioners() {
    this.service.getPractitioners()
    .subscribe((data:PractitionerModel[]) =>{
      console.log(data);
      this.practitionerArr$=data
    }
    );
  }

  selectPractitioner(selected:PractitionerModel){
    console.log("clicked Practitioner: " + selected.id)
    this.selectedPractitioner = selected;
  }

  onPractitionerModified(hidePractitioner: boolean): void {
    console.log("Practitioner modified " + hidePractitioner);
    if (hidePractitioner) { this.selectedPractitioner = null; } 
    this.getPractitioners()
  }

  createPractitioner() {
    var newPractitioner: PractitionerModel =
      new PractitionerModel(null, 
        null,
        null, 
        null, 
        true, 
        "unknown",
        null, 
        false,
        null, 
        false, 
        null,
        null,
        null,
        null,
        null);

    this.service.addPractitioner(newPractitioner).subscribe(patient => {
      console.log("Practitioner created")
      this.onPractitionerModified(true)
    });
  }
}
